from alpine

add ./omdb_rotten_tomato_rating /usr/local/bin

entrypoint ["/usr/local/bin/omdb_rotten_tomato_rating"]

cmd ["--help"]
