package main

import (
	"github.com/kenshaw/imdb"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestShowResultRating(t *testing.T) {
	stdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	sr := imdb.SearchResult{"title", "2019", "tanyid192", "to"}
	rt_rating := Rating{"my rating", "89%"}
	ShowResult(&sr, &rt_rating)

	w.Close()
	out, _ := ioutil.ReadAll(r)
	got := string(out)
	os.Stdout = stdout
	expected := "title/2019 ➡ Rotten Tomatoes 89%"
	if !strings.Contains(got, expected) {
		t.Errorf("Expected '%s' in '%s'", expected, got)
	}
}

func TestShowResultNoRating(t *testing.T) {
	stdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	sr := imdb.SearchResult{"title", "2019", "tanyid192", "to"}
	ShowResult(&sr, nil)

	w.Close()
	out, _ := ioutil.ReadAll(r)
	got := string(out)
	os.Stdout = stdout
	expected := "title/2019 ➡ No rating info"
	if !strings.Contains(got, expected) {
		t.Errorf("Expected '%s' in '%s'", expected, got)
	}
}
