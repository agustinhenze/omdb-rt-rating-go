package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/kenshaw/imdb"
	"net/url"
	"os"
	"strings"
)

type Rating struct {
	Source string
	Value  string
}

type MovieResult struct {
	Title      string
	Year       string
	Rated      string
	Released   string
	Runtime    string
	Genre      string
	Director   string
	Writer     string
	Actors     string
	Plot       string
	Language   string
	Country    string
	Awards     string
	Poster     string
	Metascore  string
	ImdbRating string
	ImdbVotes  string
	ImdbID     string
	Type       string
	Ratings    []Rating
	DVD        string
	BoxOffice  string
	Production string
	Website    string
	Response   string
	Error      string
}

func get_rotten_tomato_rating(client *imdb.Client, imdbid string) *Rating {
	res, _ := client.Do(url.Values{
		"i":        []string{imdbid},
		"plot":     []string{"plot"},
		"tomatoes": []string{"true"},
	})
	defer res.Body.Close()

	r := &MovieResult{}
	json.NewDecoder(res.Body).Decode(r)
	for _, rating := range r.Ratings {
		if rating.Source == "Rotten Tomatoes" {
			return &rating
		}
	}
	return nil
}

func ShowResult(movie *imdb.SearchResult, rt_rating *Rating) {
	output := fmt.Sprintf("%s/%s ➡", movie.Title, movie.Year)
	if rt_rating != nil {
		fmt.Println(output, "Rotten Tomatoes", rt_rating.Value)
	} else {
		fmt.Println(output, "No rating info")
	}
	fmt.Println(strings.Repeat("-", 80))
}

func main() {
	omdb_api_key := os.Getenv("OMDB_APIKEY")
	if omdb_api_key == "" {
		fmt.Fprintf(os.Stderr, "You must set the environment variable OMDB_APIKEY\n")
		os.Exit(1)
	}

	namePtr := flag.String("name", "", "Movie name you want to know the Rotten Tomato rating")
	flag.Parse()
	if *namePtr == "" {
		flag.Usage()
		os.Exit(1)
	}

	cl := imdb.New(omdb_api_key)

	res, err := cl.Search(*namePtr, "")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, movie := range res.Search {
		rt_rating := get_rotten_tomato_rating(cl, movie.ImdbID)
		ShowResult(&movie, rt_rating)
	}
}
