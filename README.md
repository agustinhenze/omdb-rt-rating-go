# omdb_rotten_tomato_rating

Get the Rotten Tomato rating for movies

## Usage

```sh
$ docker run --rm -e OMDB_APIKEY=$OMDB_APIKEY registry.gitlab.com/agustinhenze/omdb-rt-rating-go/omdb_rotten_tomato_rating
Usage of /usr/local/bin/omdb_rotten_tomato_rating:
  -name string
        Movie name you want to know the Rotten Tomato rating
```

```sh
$ docker run --rm -e OMDB_APIKEY=$OMDB_APIKEY registry.gitlab.com/agustinhenze/omdb-rt-rating-go/omdb_rotten_tomato_rating -name batman
Batman Begins/2005 ➡ Rotten Tomatoes 84%
--------------------------------------------------------------------------------
Batman v Superman: Dawn of Justice/2016 ➡ Rotten Tomatoes 28%
--------------------------------------------------------------------------------
Batman/1989 ➡ Rotten Tomatoes 71%
--------------------------------------------------------------------------------
Batman Returns/1992 ➡ Rotten Tomatoes 80%
--------------------------------------------------------------------------------
Batman Forever/1995 ➡ Rotten Tomatoes 39%
--------------------------------------------------------------------------------
Batman & Robin/1997 ➡ Rotten Tomatoes 10%
--------------------------------------------------------------------------------
The Lego Batman Movie/2017 ➡ Rotten Tomatoes 90%
--------------------------------------------------------------------------------
Batman: The Animated Series/1992–1995 ➡ No rating info
--------------------------------------------------------------------------------
Batman: Under the Red Hood/2010 ➡ Rotten Tomatoes 100%
--------------------------------------------------------------------------------
Batman: The Dark Knight Returns, Part 1/2012 ➡ Rotten Tomatoes 100%
--------------------------------------------------------------------------------
```

You can get the omdb api key from here https://omdbapi.com/apikey.aspx

## Howto build the docker image

```sh
$ docker run -e CGO_ENABLED=0 --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang bash -c "go get -d . && go build omdb_rotten_tomato_rating.go"
$ docker build -t registry.gitlab.com/agustinhenze/omdb-rt-rating-go/omdb_rotten_tomato_rating .
```
